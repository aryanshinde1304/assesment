import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Link,
  Spacer,
  Text,
  FormErrorMessage,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import {
  Link as RouterLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom";

import FormContainer from "../components/FormContainer";
// import Message from '../components/Message';

import { Controller, useForm } from "react-hook-form";

const LoginScreen = () => {
  const {
    handleSubmit,
    formState: { errors },
    control,
  } = useForm();

  const [searchParam] = useSearchParams();
  let redirect = searchParam.get("redirect") || "/";

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // const userLogin = useSelector((state) => state.userLogin);
  // const { loading, error, userInfo } = userLogin;

  // useEffect(() => {
  // 	if (userInfo) {
  // 		navigate(redirect);
  // 	}
  // }, [navigate, userInfo, redirect]);

  const submitHandler = (e) => {

  };

  return (
    <Flex w="full" alignItems="center" justifyContent="center" py="5">
      <FormContainer>
        <Heading as="h1" mb="8" fontSize="3xl">
          Login
        </Heading>

        {/* {error && <Message type='error'>{error}</Message>} */}

        <form onSubmit={handleSubmit(submitHandler)}>
          <FormControl id="email">
            <FormLabel htmlFor="email">Email address</FormLabel>
            <Controller
              rules={{
                required: "Email is required",
                pattern: {
                  value: /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/,
                  message: "Oops! We need a valid email address to proceed",
                },
              }}
              control={control}
              name="email"
              render={({ field }) => (
                <Input
                  id="email"
                  {...field}
                  type="email"
                  placeholder="username@domain.com"
                />
              )}
            />

            {errors["email"] ? (
              <div>{errors["email"].message}</div>
            ) : null}
          </FormControl>

          <Spacer h="3" />

          <FormControl id="password">
            <FormLabel htmlFor="password">Password</FormLabel>
            <Controller
              rules={{
                required: "Password is required",
              }}
              control={control}
              name="password"
              render={({ field }) => (
                <Input
				  minLength={8}
				  maxLength={20}
                  id="password"
                  {...field}
                  type="password"
                  placeholder="********"
                />
              )}
            />

			{errors["password"] ? (
              <div>{errors["password"].message}</div>
            ) : null}
          </FormControl>

          <Button type="submit" colorScheme="teal" mt="4">
            Login
          </Button>
        </form>

        <Flex pt="10">
          <Text fontWeight="semibold">
            New Customer?{" "}
            <Link as={RouterLink} to="/register">
              Click here to register
            </Link>
          </Text>
        </Flex>
      </FormContainer>
    </Flex>
  );
};

export default LoginScreen;
