import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Link,
  Spacer,
  Text,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";

import {
  Link as RouterLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import FormContainer from "../components/FormContainer";
// import Message from './components/Message';


import { Controller, useForm } from "react-hook-form";

const RegisterScreen = () => {

	const {
		handleSubmit,
		formState: { errors },
		control,
	  } = useForm();

  let [searchParams] = useSearchParams();
  let redirect = searchParams.get("redirect") || "/";

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [message, setMessage] = useState(null);

  // useEffect(() => {
  // 	if (userInfo) {
  // 		navigate(redirect);
  // 	}
  // }, [navigate, userInfo, redirect]);

  const submitHandler = (e) => {
    if (password !== confirmPassword) {
      setMessage("Passwords do not match");
    } else {
      // dispatch(register(name, email, password));
    }
  };

  return (
    <Flex w="full" alignItems="center" justifyContent="center" py="5">
      <FormContainer>
        <Heading as="h1" mb="8" fontSize="3xl">
          Register
        </Heading>

        {/* {error && <Message type='error'>{error}</Message>} */}
        {/* {message && <Message type='error'>{message}</Message>} */}

        <form onSubmit={handleSubmit(submitHandler)}>
		      <FormControl id="name">
            <FormLabel htmlFor="name">Name</FormLabel>
            <Controller
              rules={{
                required: "Name is required",
              }}
              control={control}
              name="name"
              render={({ field }) => (
                <Input
                  id="name"
                  {...field}
                  type="text"
                  placeholder="John Doe"
                />
              )}
            />

            {errors["name"] ? <div>{errors["name"].message}</div> : null}
          </FormControl>

          <Spacer h="3" />

          <FormControl id="hobby">
            <FormLabel htmlFor="hobby">Hobby</FormLabel>
            <Controller
              rules={{
                required: "Hobby is required",
              }}
              control={control}
              name="hobby"
              render={({ field }) => (
                <Input
                  id="hobby"
                  {...field}
                  type="text"
                  placeholder="Playing cricket"
                />
              )}
            />

            {errors["hobby"] ? <div>{errors["hobby"].message}</div> : null}
          </FormControl>

          <Spacer h="3" />

          <FormControl id="gender">
            <FormLabel htmlFor="gender">Gender</FormLabel>
            <Controller
              rules={{
                required: "Gender is required",
              }}
              control={control}
              name="gender"
              render={({ field }) => (
                <Input
                  id="gender"
                  {...field}
                  type="text"
                  placeholder="John Doe"
                />
              )}
            />

            {errors["gender"] ? <div>{errors["gender"].message}</div> : null}
          </FormControl>

          <Spacer h="3" />

          <FormControl id="email">
            <FormLabel htmlFor="email">Email address</FormLabel>
            <Controller
              rules={{
                required: "Email is required",
                pattern: {
                  value: /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/,
                  message: "Oops! We need a valid email address to proceed",
                },
              }}
              control={control}
              name="email"
              render={({ field }) => (
                <Input
                  id="email"
                  {...field}
                  type="email"
                  placeholder="username@domain.com"
                />
              )}
            />

            {errors["email"] ? <div>{errors["email"].message}</div> : null}
          </FormControl>

          <Spacer h="3" />

          <FormControl id="password">
            <FormLabel htmlFor="password">Password</FormLabel>
            <Controller
              rules={{
                required: "Password is required",
              }}
              control={control}
              name="password"
              render={({ field }) => (
                <Input
				  minLength={8}
				  maxLength={20}
                  id="password"
                  {...field}
                  type="password"
                  placeholder="********"
                />
              )}
            />

			{errors["password"] ? (
              <div>{errors["password"].message}</div>
            ) : null}
          </FormControl>

          <Spacer h="3" />

          <FormControl id="confirmPassword">
            <FormLabel htmlFor="confirmPassword">Confirm Password</FormLabel>
            <Controller
              rules={{
                required: "Confirm password is required",
              }}
              control={control}
              name="confirmPassword"
              render={({ field }) => (
                <Input
				  minLength={8}
				  maxLength={20}
                  id="confirmPassword"
                  {...field}
                  type="password"
                  placeholder="********"
                />
              )}
            />

			{errors["confirmPassword"] ? (
              <div>{errors["confirmPassword"].message}</div>
            ) : null}
          </FormControl>

          <Button type="submit" colorScheme="teal" mt="4">
            Register
          </Button>
        </form>

        <Flex pt="10">
          <Text fontWeight="semibold">
            Already a Customer?{" "}
            <Link as={RouterLink} to="/">
              Click here to login
            </Link>
          </Text>
        </Flex>
      </FormContainer>
    </Flex>
  );
};

export default RegisterScreen;
