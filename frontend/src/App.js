import { Flex } from '@chakra-ui/react';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';

const App = () => {
  return (
   <>
   <BrowserRouter>
   <Flex
				as='main'
				mt='72px'
				direction='column'
				py='6'
				px='6'
				bgColor='gray.200'>
          <Routes>
            <Route path="/" exact element={<LoginScreen />} />
            <Route path="/register" element={<RegisterScreen />} />
          </Routes>
          </Flex>
   </BrowserRouter>
   </>
  );
}

export default App;
