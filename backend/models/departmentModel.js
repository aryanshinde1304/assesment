
import mongoose from 'mongoose';

const departmentSchema = mongoose.Schema(
	{
		name: {
			type: String,
			required: true,
		},
		category: {
			type: String,
			required: true
		  },
		  location: {
			type: String,
			required: true  
		  },
		  salary: {
			type: Number,
			required: true
		  }
	},

);

const Department = mongoose.model('Department', departmentSchema);

export default Department;
