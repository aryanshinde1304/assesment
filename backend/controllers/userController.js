import asyncHandler from 'express-async-handler';

import User from '../models/userModel.js';
import generateToken from '../utils/generateToken.js';

/**
 * @desc		Login user
 * @router	GET /api/users/login
 * @access	public
 */
const authUser = asyncHandler(async (req, res) => {
	const { email, password } = req.body;

	const user = await User.findOne({ email });

	if (user && (await user.matchPassword(password))) {
		res.json({
			_id: user._id,
			name: user.name,
			email: user.email,
			isAdmin: user.isAdmin,
			token: generateToken(user._id),
		});
	} else {
		res.status(401);
		throw new Error('Incorrect email or password');
	}
});

/**
 * @desc		Get user profile
 * @router	GET /api/users/profile
 * @access	private
 */
const getUserProfile = asyncHandler(async (req, res) => {
	const user = await User.findById(req.user._id);

	if (user) {
		res.json({
			_id: user._id,
			name: user.name,
			email: user.email,
			isAdmin: user.isAdmin,
		});
	} else {
		res.status(404);
		throw new Error('User not found');
	}
});

/**
 * @desc		Register a new user
 * @router	POST /api/users/
 * @access	public
 */
const registerUser = asyncHandler(async (req, res) => {
	const { name, email, password, gender, hobbies } = req.body;

	const userExists = await User.findOne({ email });

	if (userExists) {
		throw new Error('User already exists');
	}

	const user = await User.create({ name, email, password, gender, hobbies });

	if (user) {
		res.json({
			_id: user._id,
			name: user.name,
			email: user.email,
			gender: user.gender,
			hobbies: user.hobbies,
			isManager: user.isAdmin,
			token: generateToken(user._id),
		});
	} else {
		res.status(400); // BAD REQUEST
		throw new Error('Invalid user data');
	}
});

/**
 * @desc		Update user profile
 * @router	PUT /api/users/profile
 * @access	private
 */
const updateUserProfile = asyncHandler(async (req, res) => {
	const user = await User.findById(req.user._id);

	if (user) {
		user.name = req.body.name || user.name;
		user.email = req.body.email || user.email;
		if (req.body.password) {
			user.password = req.body.password;
		}

		await user.save();

		res.json({
			_id: user._id,
			name: user.name,
			email: user.email,
			isAdmin: user.isAdmin,
			token: generateToken(user._id),
		});
	} else {
		res.status(404);
		throw new Error('User not found');
	}
});

/**
 * @desc		Get all users
 * @router	GET /api/users/
 * @access	private/admin
 */
const getUsers = asyncHandler(async (req, res) => {
	const users = await User.find({}).select('-password');
	res.json(users);
});

/**
 * @desc		Delete user
 * @router	DELETE /api/users/:id
 * @access	private/admin
 */
const deleteUser = asyncHandler(async (req, res) => {
	const user = await User.findById(req.params.id);

	if (user) {
		await User.deleteOne(user);
		res.json({ message: 'User deleted' });
	} else {
		res.status(404);
		throw new Error('User not found');
	}
});

/**
 * @desc		Get user by ID
 * @router	GET /api/users/:id
 * @access	private/admin
 */
const getUserByID = asyncHandler(async (req, res) => {
	const user = await User.findById(req.params.id).select('-password');

	if (user) {
		res.json(user);
	} else {
		res.status(404);
		throw new Error('User not found');
	}
});

/**
 * @desc		Update a user
 * @router	PUT /api/users/:id
 * @access	private/admin
 */
const updateUser = asyncHandler(async (req, res) => {
	const user = await User.findById(req.params.id);

	if (user) {
		user.name = req.body.name || user.name;
		user.email = req.body.email || user.email;
		user.isAdmin = req.body.isAdmin;

		const updatedUser = await user.save();

		res.json({
			_id: updatedUser._id,
			name: updatedUser.name,
			email: updatedUser.email,
			isAdmin: updatedUser.isAdmin,
		});
	}
});

const getAllUsers = asyncHandler(async (req, res) => {
	const { page, limit, location, name, department, category } = req.query;
  
	// Define the filters for the query
	const filters = { isManager: false };
	
	if (location) {
	  filters.location = location;
	}
  
	if (name) {
	  filters.name = { $regex: new RegExp(name, 'i') };
	}
  
	if (department) {
	  filters.department = department;
	}
  
	if (category) {
	  filters.category = category;
	}
  
	// Parse page and limit values or use defaults
	const parsedPage = parseInt(page, 10) || 1;
	const parsedLimit = parseInt(limit, 10) || 10;
  
	// Calculate the skip value to implement pagination
	const skip = (parsedPage - 1) * parsedLimit;
  
	try {
	  const users = await User.find(filters)
		.skip(skip)
		.limit(parsedLimit)
		.exec();
  
	  const totalUsers = await User.countDocuments(filters).exec();
  
	  const totalPages = Math.ceil(totalUsers / parsedLimit);
  
	  res.status(200).json({
		users,
		page: parsedPage,
		limit: parsedLimit,
		totalPages,
		totalUsers,
	  });
	} catch (error) {
	  res.status(500).json({ error: 'Internal Server Error' });
	}
  });

export {
	authUser,
	deleteUser,
	getUserByID,
	getUserProfile,
	getUsers,
	registerUser,
	updateUser,
	updateUserProfile,
	getAllUsers,
};
