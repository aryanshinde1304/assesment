import Joi from 'joi';

// @desc Login Validation

const loginValidate = {
  body: Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().min(8).max(20).required()
  })
};


// @desc Register Validation

const registerValidate = {
    body: Joi.object().keys({
      name: Joi.string().required(),
      email: Joi.string().email().lowercase(),
      password: Joi.string()
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]/)
        .min(8)
        .max(20)
        .required(),
      confirmPassword: Joi.string().equal(Joi.ref('password')),
    })
  };
  
  export { loginValidate, registerValidate };